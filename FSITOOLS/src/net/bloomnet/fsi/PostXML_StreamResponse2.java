package net.bloomnet.fsi;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;


public class PostXML_StreamResponse2 {

		public PostXML_StreamResponse2() throws Exception {
			
			File myOutFile = new File("xmlResults.csv");
			FileOutputStream fos = new FileOutputStream(myOutFile);
			Map<String,Integer> map = new HashMap<String,Integer>();
			
			for(int ii=0; ii < 100; ++ii){
				
				String myURL = "http://bloomlink.net/fsiv2/processor";		//external QA
				String myFunc = "getMemberDirectory";
		
				String myXMLString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><memberDirectoryInterface><searchShopRequest><security><username>A222</username><password>BMS</password><shopCode>A2220000</shopCode></security><memberDirectorySearchOptions><searchAvailability><shopDetails>Y</shopDetails><deliveryDate>10/30/2019</deliveryDate><zipCode>77001</zipCode><excludedShops><excludedShopCodes>X3460000</excludedShopCodes></excludedShops></searchAvailability></memberDirectorySearchOptions></searchShopRequest></memberDirectoryInterface>";
				//String myXMLString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><memberDirectoryInterface><searchShopRequest><security><username>A222</username><password>qa</password><shopCode>A2220000</shopCode></security><memberDirectorySearchOptions><searchShopCodeByDeliveryDateAndZipCode><deliveryDate>10/15/2019</deliveryDate><zipCode>99998</zipCode></searchShopCodeByDeliveryDateAndZipCode></memberDirectorySearchOptions></searchShopRequest></memberDirectoryInterface>";	
				
				PostMethod myPost = new PostMethod(myURL);
				
				myPost.addParameter("func", myFunc);
				myPost.addParameter("data", myXMLString);
				
				HttpClient myClient = new HttpClient();
				
				try {
					myClient.executeMethod(myPost);
					String response = myPost.getResponseBodyAsString();
					
					//System.out.println(response);
					
					String shopCode = response.split("<shopCode>")[1].split("</shopCode>")[0];
					
		            System.out.println(shopCode);
		            if(map.get(shopCode) == null){
		            	map.put(shopCode,1);
		            }else{
		            	Integer currVal = map.get(shopCode);
		            	currVal++;
		            	map.put(shopCode,currVal);
		            }
		            
		            fos.write((shopCode + "\r\n").getBytes());
		            fos.flush();
	
		        }catch(Exception ee){
		        	ee.printStackTrace();
		        }
				finally {
		        
		           				
					myPost.releaseConnection();
					
				}
			}
			fos.close();
			System.out.println(map.toString());
			
		}
		
		public static void main(String[] args){
			try {
				new PostXML_StreamResponse2();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
}
