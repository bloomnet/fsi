package net.bloomnet.fsi;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;


public class TripTime {
	
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");

		public TripTime() throws Exception {

			while(true){
				String myURL = "http://www.bloomlink.net/fsiv2/processor";	//external PROD
	
				//FSI Function leveraged
				String myFunc = "getMemberDirectory";
				
				String myXMLString = "<memberDirectoryInterface><searchShopRequest><security><username>DIALER</username><password>TRUNK</password><shopCode>X0690000</shopCode></security><memberDirectorySearchOptions><searchByAvailabilityDate><availabilityDate>11/15/2019</availabilityDate><zipCode>10018</zipCode><city/><state/></searchByAvailabilityDate></memberDirectorySearchOptions></searchShopRequest></memberDirectoryInterface>";
					
				//CREATE OBJECT TO POST
				PostMethod myPost = new PostMethod(myURL);
				
				myPost.addParameter("func", myFunc);
				myPost.addParameter("data", myXMLString);
				
				//START BROWSER
				HttpClient myClient = new HttpClient();
				InputStream myStream = null;
				
				//FEED THE FSI AND DISPLAY RESPONSE!
				try {
					
					System.out.println("Start Time: " + dateFormat.format(new Date()));
					long startTime = System.currentTimeMillis();
					
					myClient.executeMethod(myPost);
					
					System.out.println("Status Code 200: OK");
					
					long elaspsedTime = System.currentTimeMillis() - startTime;
					System.out.println("End Time: " + dateFormat.format(new Date()));
					System.out.println("Turnaround time: " + elaspsedTime + "ms\n");
	
	
		        }catch(Exception ee){ee.printStackTrace();}
					
					myPost.releaseConnection();
					Thread.sleep(2000);
			}
		}
			
		
		public static void main(String[] args){
			try {
				new TripTime();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
			
}

