package net.bloomnet.fsi;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;


public class ParseDirectory {


	public ParseDirectory() {

		final String myDir = "/opt/data/fsi/";
		
		Date today = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String date = sdf.format(today);

		String myXMLFile = "MemberDirectory_wTLO_"+date+".xml";

		final String myOutFile = "MemberDirectory_wTLO_"+date+"_coverage.txt";
		final String myOutFile2 = "MemberDirectory_wTLO_"+date+"_ziplisting.txt";
		final String myHeaderString = "shopCode	name	attention	addressLine1	addressLine2	city	state	zip	countryCode	phoneNumber	faxNumber	email	shopStatus	communicationCode	sundayIndicator";

		
		try {
			
			File file = new File(myDir+myXMLFile);
			
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			String line;
			
			PrintWriter fos = new PrintWriter(myDir+myOutFile, "UTF-8");
			PrintWriter fos2 = new PrintWriter(myDir+myOutFile2, "UTF-8");
			
			fos.write(myHeaderString);
			
			while ((line = bufferedReader.readLine()) != null) {
				
				String [] lineTemp = line.split("<shop>");
				
				for(int ii=1; ii<lineTemp.length; ++ii ){
					
					String shopCode = lineTemp[ii].split("<shopCode>")[1].split("</shopCode>")[0];
					String shopName = lineTemp[ii].split("<name>")[1].split("</name>")[0];
					String attention = lineTemp[ii].split("<attention>")[1].split("</attention>")[0];
					String addressLine1 = lineTemp[ii].split("<addressLine1>")[1].split("</addressLine1>")[0];
					String addressLine2 = lineTemp[ii].split("<addressLine2>")[1].split("</addressLine2>")[0];
					String city = lineTemp[ii].split("<city>")[1].split("</city>")[0];
					String state = lineTemp[ii].split("<state>")[1].split("</state>")[0];
					String zip = lineTemp[ii].split("<zip>")[1].split("</zip>")[0];
					String countryCode = lineTemp[ii].split("<countryCode>")[1].split("</countryCode>")[0];
					String phoneNumber = lineTemp[ii].split("<phoneNumber>")[1].split("</phoneNumber>")[0];
					String faxNumber = lineTemp[ii].split("<faxNumber>")[1].split("</faxNumber>")[0];
					String email = lineTemp[ii].split("<email>")[1].split("</email>")[0];
					String shopStatus = lineTemp[ii].split("<shopStatus>")[1].split("</shopStatus>")[0];
					String communicationCode = lineTemp[ii].split("<communicationCode>")[1].split("</communicationCode>")[0];
					String sundayIndicator = lineTemp[ii].split("<sundayIndicator>")[1].split("</sundayIndicator>")[0];
					String zipsListFinal = "";
					
					String [] serviced = lineTemp[ii].split("<servicedZips>");
					
					if(serviced.length > 1){
						String servicedZips = lineTemp[ii].split("<servicedZips>")[1].split("</servicedZips>")[0];
						servicedZips = servicedZips.replaceAll("<zip></zip>", "");
						String[] zipList = servicedZips.split("<zip>");
												
						if(zipList.length > 1){
							for(int xx=1; xx<zipList.length; ++xx){
								if(xx == 1)
									zipsListFinal += zipList[xx].split("</zip>")[0];
								else
									zipsListFinal += "," + zipList[xx].split("</zip>")[0];
								fos2.write(shopCode + "\t" + zipList[xx].split("</zip>")[0] + "\r\n");
							}
						}
					}
					
					fos.write(shopCode + "\t" + shopName + "\t" + attention + "\t" + addressLine1 + "\t" + addressLine2 + "\t" + city + "\t" + state + "\t" + zip + "\t" + 
					countryCode + "\t" + phoneNumber + "\t" + faxNumber + "\t" + email + "\t" + shopStatus + "\t" + communicationCode + "\t" + sundayIndicator + "\t" + zipsListFinal + "\t\r\n");
					

					System.out.println(shopCode + " added.");
				}

			}
			
			fileReader.close();
			fos.flush();
			fos.close();
			fos2.flush();
			fos2.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args){
		try {
			new PostXML_StreamResponse();
		} catch (Exception e) {
			e.printStackTrace();
		}
		new ParseDirectory();
	}

}


