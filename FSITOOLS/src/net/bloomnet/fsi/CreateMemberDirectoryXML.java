package net.bloomnet.fsi;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;


public class CreateMemberDirectoryXML {


	public CreateMemberDirectoryXML() throws IOException {

		String myDir = "/root/";
		String myURL = "http://www.bloomlink.net/fsiv2/processor";	//external PROD

		String myFunc = "getMemberDirectory";
		
		String myFile = "fsi_member_directory_SearchAll_wTLO.xml";
		
		File myXML = new File(myDir + myFile);

		File myOutFile = new File("memberDirectory.xml");
		
		//READ FILE INTO A STRING
		StringBuffer buffer = new StringBuffer();
		FileInputStream myXMLStream = new FileInputStream(myXML);
		InputStreamReader ISR = new InputStreamReader(myXMLStream);
		Reader in = new BufferedReader(ISR);
		
		//LOOP THROUGH THE FILE AND ADD IT TO THE BUFFER
		int ch;
		while ((ch = in.read()) > -1) {
			buffer.append((char)ch);
		}
		in.close();
		
		//CONVERT BUFFER TO A STRING
		String myXMLString = buffer.toString();
			
		//CREATE OBJECT TO POST
		PostMethod myPost = new PostMethod(myURL);
		
		myPost.addParameter("func", myFunc);
		myPost.addParameter("data", myXMLString);
		
		
		//START BROWSER
		HttpClient myClient = new HttpClient();
		FileOutputStream outFile = null;
		InputStream myStream = null;
		
		//FEED THE FSI AND DISPLAY RESPONSE!
		try {
			int result = myClient.executeMethod(myPost);
			
			System.out.println("Response status code: " + result);
			
			System.out.println("Response Body: ");
			System.out.println("Getting response as Stream...");
			myStream = myPost.getResponseBodyAsStream();

			outFile = new FileOutputStream(myDir + myOutFile);
			int c;

            while ((c = myStream.read()) != -1) {
            	outFile.write(c);
            }

        } finally {
            if (in != null) {
                in.close();
            }
            if (outFile != null) {
            	outFile.close();
            }

			
			myPost.releaseConnection();
			System.out.println("Done!");
			
		}
		
	}
		
	
	public static void main(String[] args){
		try {
			new CreateMemberDirectoryXML();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}


