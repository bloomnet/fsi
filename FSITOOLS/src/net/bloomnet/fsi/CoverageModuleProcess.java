package net.bloomnet.fsi;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class CoverageModuleProcess {


	public CoverageModuleProcess() {

		final String myDir = "/opt/data/fsi/";
		
		Date today = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String date = sdf.format(today);

		String myXMLFile = "MemberDirectory_wTLO_"+date+".xml";

		SQLData mySQL = new SQLData();
		
		String deleteStatement = "DELETE FROM zip_coverage;";
		mySQL.executeStatement(deleteStatement);
		
		try {
			
			File file = new File(myDir+myXMLFile);
			
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			String line;
			
			while ((line = bufferedReader.readLine()) != null) {
				
				String [] lineTemp = line.split("<shop>");
				
				for(int ii=1; ii<lineTemp.length; ++ii ){
					
					String shopCode = lineTemp[ii].split("<shopCode>")[1].split("</shopCode>")[0];
					
					String [] serviced = lineTemp[ii].split("<servicedZips>");
					
					if(serviced.length > 1){
						String servicedZips = lineTemp[ii].split("<servicedZips>")[1].split("</servicedZips>")[0];
						servicedZips = servicedZips.replaceAll("<zip></zip>", "");
						String[] zipList = servicedZips.split("<zip>");
												
						if(zipList.length > 1){
							for(int xx=1; xx<zipList.length; ++xx){
								String insertStatement = "INSERT INTO zip_coverage(shop_code,zip) VALUES(\""+shopCode+"\",\""+zipList[xx].split("</zip>")[0]+"\");";
								mySQL.executeStatement(insertStatement);
							}
						}
					}
					
				}

			}
			
			fileReader.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args){
		try {
			new PostXML_StreamResponse();
		} catch (Exception e) {
			e.printStackTrace();
		}
		new CoverageModuleProcess();
	}
}