import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class parseMemberDirectorytXML_SAX2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		final String myDir = "C:\\Documents and Settings\\u0932\\My Documents\\aWork\\Software\\FSI\\JohnsApp\\";


		//String myXMLFile = "MemberDirectory_20111213.xml";
		//String myXMLFile = "MemberDirectory_20111221.xml";
		//String myXMLFile = "MemberDirectory_20120107.xml";
		//String myXMLFile = "MemberDirectory_20120119.xml";
		//String myXMLFile = "MemberDirectory_20120131.xml";
		//String myXMLFile = "MemberDirectory_20120206.xml";
		//String myXMLFile = "MemberDirectory_20120228.xml";
		//String myXMLFile = "MemberDirectory_20120403.xml";
		//String myXMLFile = "MemberDirectory_20120504.xml";
		//String myXMLFile = "MemberDirectory_QA_20120521_linesbFLAT.txt.xml";
		//String myXMLFile = "MemberDirectory_20120521.xml";
		//String myXMLFile = "MemberDirectory_20120522_QA_old_linesbFLAT.xml";
		//String myXMLFile = "mini.txt";
		//String myXMLFile = "MemberDirectory_wTLO_20120613.xml";
		//String myXMLFile = "MemberDirectory_wTLO_20120727.xml";
		//String myXMLFile = "MemberDirectory_wTLO_20120905.xml";
		String myXMLFile = "MemberDirectory_wTLO_20120928.xml";
		
		//String myXMLFile = "smallMemberDir.txt";
		//final String myOutFile = "ztest_saxout.txt";
		//final String myOutFile = "MemberDirectory_20111221_coverage2.txt";
		//final String myOutFile2 = "MemberDirectory_20111221_ziplisting.txt";
		//final String myOutFile = "MemberDirectory_20120119_coverage.txt";
		//final String myOutFile2 = "MemberDirectory_20120119_ziplisting.txt";
		//final String myOutFile = "MemberDirectory_20120119_coverage_NON_US.txt";
		//final String myOutFile2 = "MemberDirectory_20120119_ziplisting_NON_US.txt";
		//final String myOutFile = "MemberDirectory_20120131_coverage_NON_US.txt";
		//final String myOutFile2 = "MemberDirectory_20120131_ziplisting_NON_US.txt";
		//final String myOutFile = "MemberDirectory_20120206_coverageZ.txt";
		//final String myOutFile2 = "MemberDirectory_20120206_ziplistingZ.txt";
		//final String myOutFile = "MemberDirectory_20120228_coverage.txt";
		//final String myOutFile2 = "MemberDirectory_20120228_ziplisting.txt";
		//final String myOutFile = "MemberDirectory_20120403_coverage.txt";
		//final String myOutFile2 = "MemberDirectory_20120403_ziplisting.txt";
		//final String myOutFile = "MemberDirectory_QA_20120521b_coverage.txt";
		//final String myOutFile2 = "MemberDirectory_QA_20120521b_ziplisting.txt";
		//final String myOutFile = "MemberDirectory_20120521_coverage.txt";
		//final String myOutFile2 = "MemberDirectory_20120521_ziplisting.txt";
		//final String myOutFile = "MemberDirectory_20120522_QA_old_coverage.txt";
		//final String myOutFile2 = "MemberDirectory_20120522_QA_old_ziplisting.txt";
		//final String myOutFile = "mini_coverage.txt";
		//final String myOutFile2 = "mini_ziplisting.txt";
		//final String myOutFile = "MemberDirectory_wTLO_20120613_coverage.txt";
		//final String myOutFile2 = "MemberDirectory_wTLO_20120613_ziplisting.txt";
		//final String myOutFile = "MemberDirectory_wTLO_20120727_coverage.txt";
		//final String myOutFile2 = "MemberDirectory_wTLO_20120727_ziplisting.txt";
		//final String myOutFile = "MemberDirectory_wTLO_20120905_coverage.txt";
		//final String myOutFile2 = "MemberDirectory_wTLO_20120905_ziplisting.txt";
		final String myOutFile = "MemberDirectory_wTLO_20120928_coverage.txt";
		final String myOutFile2 = "MemberDirectory_wTLO_20120928_ziplisting.txt";
		
		
		
		final String myHeaderString = "shopCode	name	attention	addressLine1	addressLine2	city	state	zip	countryCode	phoneNumber	faxNumber	email	shopStatus	communicationCode	sundayIndicator";

		//flatten
		
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();

			DefaultHandler handler = new DefaultHandler() {
				boolean servicedZips = false;
				boolean shopCode = false;
				boolean name = false;
				boolean attention = false;
				boolean addressLine1 = false;
				boolean addressLine2 = false;
				boolean city = false;
				boolean state = false;
				boolean zip = false;
				boolean countryCode = false;
				boolean phoneNumber = false;
				boolean faxNumber = false;
				boolean email = false;
				boolean shopStatus = false;
				boolean communicationCode = false;
				boolean sundayIndicator = false;
				Hashtable myShop = new Hashtable();
				List myZips = new ArrayList<String>();
				int j;

				public void startElement(String uri, String localName,
						String qName, Attributes attributes)
				throws SAXException {

					if (qName.equalsIgnoreCase("shop")) {
						myShop.put("shopCode", "");
						myShop.put("name", "");
						myShop.put("attention", "");
						myShop.put("addressLine1", "");
						myShop.put("addressLine2", "");
						myShop.put("city", "");
						myShop.put("state", "");
						myShop.put("zip", "");
						myShop.put("countryCode", "");
						myShop.put("phoneNumber", "");
						myShop.put("faxNumber", "");
						myShop.put("email", "");
						myShop.put("shopStatus", "");
						myShop.put("communicationCode", "");
						myShop.put("sundayIndicator", "");
						myZips.clear();
						servicedZips = false;
					}
					if (qName.equalsIgnoreCase("shopCode")) {
						shopCode = true;
					}
					if (qName.equalsIgnoreCase("name")) {
						name = true;
					}
					if (qName.equalsIgnoreCase("attention")) {
						attention = true;
					}
					if (qName.equalsIgnoreCase("addressLine1")) {
						addressLine1 = true;
					}
					if (qName.equalsIgnoreCase("addressLine2")) {
						addressLine2 = true;
					}
					if (qName.equalsIgnoreCase("city")) {
						city = true;
					}
					if (qName.equalsIgnoreCase("state")) {
						state = true;
					}
					if (qName.equalsIgnoreCase("zip")) {
						zip = true;
					}
					if (qName.equalsIgnoreCase("countryCode")) {
						countryCode = true;
					}
					if (qName.equalsIgnoreCase("phoneNumber")) {
						phoneNumber = true;
					}
					if (qName.equalsIgnoreCase("faxNumber")) {
						faxNumber = true;
					}
					if (qName.equalsIgnoreCase("email")) {
						email = true;
					}
					if (qName.equalsIgnoreCase("shopStatus")) {
						shopStatus = true;
					}
					if (qName.equalsIgnoreCase("communicationCode")) {
						communicationCode = true;
					}
					if (qName.equalsIgnoreCase("sundayIndicator")) {
						sundayIndicator = true;
					}
					if (qName.equalsIgnoreCase("servicedZips")) {
						servicedZips = true;
						System.out.println("servicing zips is true");
					}
				}

				public void endElement(String uri, String localName,
						String qName)
				throws SAXException {
					//if (qName.equalsIgnoreCase("shop") && !(myShop.get("countryCode").equals("USA") || myShop.get("countryCode").equals("US"))) {
					//if (qName.equalsIgnoreCase("shop") && (myShop.get("countryCode").equals("USA") || myShop.get("countryCode").equals("US"))) {
						if (qName.equalsIgnoreCase("shop") ) {
						try {
								
							FileOutputStream fos;
							FileOutputStream fos2;
							fos = new FileOutputStream(myDir + myOutFile,true);
							fos2 = new FileOutputStream(myDir + myOutFile2,true);
						System.out.println(myShop.get("shopCode").toString() + "\nadd2: " + myShop.get("addressLine2").toString());
							fos.write(myShop.get("shopCode").toString().getBytes());
						fos.write("\t".getBytes());
						fos.write(myShop.get("name").toString().getBytes());
						fos.write("\t".getBytes());
						fos.write(myShop.get("attention").toString().getBytes());
						fos.write("\t".getBytes());
						fos.write(myShop.get("addressLine1").toString().getBytes());
						fos.write("\t".getBytes());
						fos.write(myShop.get("addressLine2").toString().getBytes());
						fos.write("\t".getBytes());
						fos.write(myShop.get("city").toString().getBytes());
						fos.write("\t".getBytes());
						fos.write(myShop.get("state").toString().getBytes());
						fos.write("\t".getBytes());
						fos.write(myShop.get("zip").toString().getBytes());
						fos.write("\t".getBytes());
						fos.write(myShop.get("countryCode").toString().getBytes());
						fos.write("\t".getBytes());
						fos.write(myShop.get("phoneNumber").toString().getBytes());
						fos.write("\t".getBytes());
						fos.write(myShop.get("faxNumber").toString().getBytes());
						fos.write("\t".getBytes());
						fos.write(myShop.get("email").toString().getBytes());
						fos.write("\t".getBytes());
						fos.write(myShop.get("shopStatus").toString().getBytes());
						fos.write("\t".getBytes());
						fos.write(myShop.get("communicationCode").toString().getBytes());
						fos.write("\t".getBytes());
						fos.write(myShop.get("sundayIndicator").toString().getBytes());
						fos.write("\t".getBytes());
						if(myZips.size() > 0) {
							System.out.println(myZips.size());

							String oneZip = myZips.get(0).toString();

							fos2.write(myShop.get("shopCode").toString().getBytes());
							fos2.write("\t".getBytes());
							fos2.write(oneZip.getBytes());
							fos2.write("\n".getBytes());
							
							fos.write(oneZip.getBytes());
							oneZip = "";

							for(int i=1; i < myZips.size(); i++) {
								oneZip = "";

								oneZip = myZips.get(i).toString();
								fos.write(",".getBytes());
								fos.write(oneZip.getBytes());

								fos2.write(myShop.get("shopCode").toString().getBytes());
								fos2.write("\t".getBytes());
								fos2.write(oneZip.getBytes());
								fos2.write("\n".getBytes());

							}
						}
						else {
							fos.write("\t".getBytes());							
						}
						fos.write("\n".getBytes());
						fos.close();
						myShop.clear();
						myZips.clear();
						j = 0;
						}
						catch (Exception e){
							
						}
					}					
					if (qName.equalsIgnoreCase("servicedZips")) {
						servicedZips = false;
					}
					if (qName.equalsIgnoreCase("shopCode")) {
						shopCode = false;
					}
					if (qName.equalsIgnoreCase("name")) {
						name = false;
					}
					if (qName.equalsIgnoreCase("attention")) {
						attention = false;
					}
					if (qName.equalsIgnoreCase("addressLine1")) {
						addressLine1 = false;
					}
					if (qName.equalsIgnoreCase("addressLine2")) {
						addressLine2 = false;
					}
					if (qName.equalsIgnoreCase("city")) {
						city = false;
					}
					if (qName.equalsIgnoreCase("state")) {
						state = false;
					}
					if (qName.equalsIgnoreCase("zip")) {
						zip = false;
					}
					if (qName.equalsIgnoreCase("countryCode")) {
						countryCode = false;
					}
					if (qName.equalsIgnoreCase("phoneNumber")) {
						phoneNumber = false;
					}
					if (qName.equalsIgnoreCase("faxNumber")) {
						faxNumber = false;
					}
					if (qName.equalsIgnoreCase("email")) {
						email = false;
					}
					if (qName.equalsIgnoreCase("shopStatus")) {
						shopStatus = false;
					}
					if (qName.equalsIgnoreCase("communicationCode")) {
						communicationCode = false;
					}
					if (qName.equalsIgnoreCase("sundayIndicator")) {
						sundayIndicator = false;
					}
				}

				public void characters(char ch[], int start, int length)
				throws SAXException {
					if (shopCode) {
						String myData = new String(ch, start, length);
						myShop.put("shopCode", myData);
						shopCode = false;
					}
					if (name) {
						String myData = new String(ch, start, length);
						myShop.put("name", myData);
						name = false;
					}
					if (attention) {
						String myData = new String(ch, start, length);
						myShop.put("attention", myData);
						attention = false;
					}
					if (addressLine1) {
						String myData = new String(ch, start, length);
						myShop.put("addressLine1", myData);
						addressLine1 = false;
					}
					if (addressLine2) {
						String myData = new String(ch, start, length);
						myShop.put("addressLine2", myData);
						addressLine2 = false;
					}
					if (city) {
						String myData = new String(ch, start, length);
						myShop.put("city", myData);
						city = false;
					}
					if (state) {
						String myData = new String(ch, start, length);
						myShop.put("state", myData);
						state = false;
					}
					if (zip == true && servicedZips == false) {
						String myData = new String(ch, start, length);
						myShop.put("zip", myData);
						zip = false;
					}
					if (zip = true && servicedZips == true) {
						String myData = new String(ch, start, length);
						myZips.add(myData.toString());
						zip = false;
					}
					if (countryCode) {
						String myData = new String(ch, start, length);
						myShop.put("countryCode", myData);
						countryCode = false;
						System.out.println(myData);
					}
					if (phoneNumber) {
						String myData = new String(ch, start, length);
						myShop.put("phoneNumber", myData);
						phoneNumber = false;
					}
					if (faxNumber) {
						String myData = new String(ch, start, length);
						myShop.put("faxNumber", myData);
						faxNumber = false;
					}
					if (email) {
						String myData = new String(ch, start, length);
						myShop.put("email", myData);
						email = false;
					}
					if (shopStatus) {
						String myData = new String(ch, start, length);
						myShop.put("shopStatus", myData);
						shopStatus = false;
					}
					if (communicationCode) {
						String myData = new String(ch, start, length);
						myShop.put("communicationCode", myData);
						communicationCode = false;
					}
					if (sundayIndicator) {
						String myData = new String(ch, start, length);
						myShop.put("sundayIndicator", myData);
						sundayIndicator = false;
					}
				}

			};

			saxParser.parse(myDir + myXMLFile, handler);
			
		} catch (Exception e){
			e.printStackTrace();
		}
	}

}
