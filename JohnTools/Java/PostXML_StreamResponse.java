import java.io.*;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;


public class PostXML_StreamResponse {

		public static void main(String[] args) throws Exception {

			//DECLARE VARIABLES
			//directory to find file
			String myDir = "C:\\Documents and Settings\\u0932\\My Documents\\aWork\\Software\\FSI\\JohnsApp\\";

			//URL to use depends on location
			//String myURL = "http://10.180.1.220:9081/fsiv2/processor"; 		//18F internal QA
			String myURL = "http://qa.bloomlink.net/fsiv2/processor";		//external QA
			//String myURL = "http://www.bloomlink.net/fsiv2/processor";	//external PROD

			//FSI Function leveraged
			//String myFunc = "getMemberDirectory";
			String myFunc = "postmessages";
			//String myFunc = "getAuditInfo";
			//String myFunc = "getmessages";
			//String myFunc = "getStatement";
			
			//XML file to send
			//String myFile = "fsi_member_directory_SearchShopCodesByAvailabilityDate.xml";
			//String myFile = "fsi_member_directory_SearchAvailabilityByShopCodesAndDeliveryDate.xml";
			//String myFile = "fsi_member_directory_SearchByAvailabilityDate.xml";
			//String myFile = "fsi_member_directory_SearchMetaStoreTypeInfo.xml";
			//String myFile = "fsi_member_directory_SearchSecureShopInfo.xml";
			//String myFile = "fsi_member_directory_SearchByShopCode_123print.xml";
			//String myFile = "fsi_member_directory_SearchByShopCode_123print_nonMember.xml";
			//String myFile = "fsi_member_directory_SearchByShopCode_123print_Universal.xml";
			//String myFile = "fsi_member_directory_SearchByShopCode_123print_NoBloomlinkIndicatorCRM.xml";
			//String myFile = "fsi_member_directory_SearchByShopCode2.xml";
			//String myFile = "fsi_member_directory_SearchAvailabilityByShopCodesAndDeliveryDate.xml";
			//String myFile = "fsi_member_directory_SearchAvailabilityByShopCodeAndDeliveryDate.xml";
			//String myFile = "fsi_member_directory_jled.txt";
			//String myFile = "fsi_inbound_order_request_jled.xml";
			//String myFile = "fsi_member_directory_jledPROD.txt";
			//String myFile = "AuditMessagesByDateRange_REQUEST.xml";
			//String myFile = "audit_AuditOrderByOrderNumber.xml";
			//String myFile = "audit_AuditOrderDetailByOrderNumber.xml";
			//String myFile = "fsi_GetMessages.xml";
			//String myFile = "fsi_messages_SendOrder3_.xml";
			//String myFile = "fsi_inbound_pchg_general_type_jled.xml";
			//String myFile = "fsi_inbound_pchg_general_type_jled2.xml";
			//String myFile = "fsi_inbound_order_request_jled2.xml";
			//String myFile = "fsi_messages_SendOrder_dec2010release_1.xml";
			//String myFile = "fsi_messages_SendOrder_dec2010release_2.xml";
			//String myFile = "fsi_messages_SendOrder_dec2010release_3.xml";
			//String myFile = "fsi_member_directory_searchShopCodeByDeliveryDateAndZipCode.xml";
			//String myFile = "fsi_inbound_order_request_jled.xml";
			//String myFile = "fsi_inbound_order_request_jled_suspended.xml";
			//String myFile = "fsi_GetMessages.xml";
			//String myFile = "audit_AuditOrderDetailByOrderNumber.xml";
			//String myFile = "fsi_inbound_pchg_message4.xml";
			//String myFile = "fsi_member_directory_SearchByShopCode_forZips.xml";
			//String myFile = "SearchByShopCode_prod.xml";
			//String myFile = "SearchAvailabilityByShopCodeAndDeliveryDate_prod.xml";
			//String myFile = "fsi_inbound_order_request_R911.xml";
			//String myFile = "fsi_inbound_order_request_SKU.xml";
			//String myFile = "fsi_member_directory_SearchByShopCode.xml";
			//String myFile = "fsi_searchFamily.xml";
			//String myFile = "dlcf_general_type.xml";
			//String myFile = "bms_order_request.xml";
			//String myFile = "fsi_inbound_inqr_general_type.xml";
			//String myFile = "BOM_Messaging_03_sendInquiry.xml";
			//String myFile = "fsi_getStatement.xml";
			//String myFile = "fsi_member_directory_SearchAll.xml";
			//String myFile = "fsi_member_directory_SearchAll_wTLO.xml";
			//String myFile = "fsi_inbound_order_request_jled.xml";
			//String myFile = "DISPUTE_106.xml";
			//String myFile = "fsi_GetMessages.xml";
			//String myFile = "DISPUTE_103.xml";
			//String myFile = "TR_test1.xml";
			String myFile = "sendOrder.xml";
			
			//OPEN UP XML FILE
			//(this can later be replaced by XMLBuilder function)
			File myXML = new File(myDir + myFile);

			//File myOutFile = new File("output.xml");
			//File myOutFile = new File("memberdirectory_20100512.xml");
			//File myOutFile = new File("FSI_SearchByAvailabilityDate_response.xml");
			//File myOutFile = new File("FSI_OrderSentToNonBloomLinkShop_response.xml");
			//File myOutFile = new File("E507_auditorder.xml");
			//File myOutFile = new File("POS_DecRelease_Resp.xml");
			//File myOutFile = new File("POS_DecRelease_Resp_2.xml");
			//File myOutFile = new File("POS_DecRelease_Resp_3.xml");
			//File myOutFile = new File("0800_info.xml");
			//File myOutFile = new File("shop_availability_S187.xml");
			//File myOutFile = new File("order_audit_testing1.xml");
			//File myOutFile = new File("order_audit_testingC1.xml");
			//File myOutFile = new File("order_audit_testingC8.xml");
			//File myOutFile = new File("fsi_message_testingA1.xml");
			//File myOutFile = new File("fsi_getStatement_Response_X069_201101.xml");
			//File myOutFile = new File("fsi_getStatement_Response_B253_201101b.xml");
			//File myOutFile = new File("fsi_getStatement_Response_X069_201102c.xml");
			//File myOutFile = new File("fsi_getStatement_Response_Y052_201102.xml");
			//File myOutFile = new File("getZips.xml");
			//File myOutFile = new File("fsi_getStatement_Response_B253_201107.xml");
			//File myOutFile = new File("fsi_getStatement_Response_3760_201102.xml");
			//File myOutFile = new File("fsi_getStatement_Response_2060_201102.xml");
			//File myOutFile = new File("order_response_2_closed_shop.xml");
			//File myOutFile = new File("getstatement_x069_201103.xml");
			//File myOutFile = new File("getstatement_p539_201103.xml");
			//File myOutFile = new File("getstatement_e507_201103.xml");
			//File myOutFile = new File("getstatement_b253_201104.xml");
			//File myOutFile = new File("getstatement_c573_201103.xml");
			//File myOutFile = new File("Y084_availability2.xml");
			//File myOutFile = new File("getstatement_y052_201103.xml");
			//File myOutFile = new File("getStatementTest.xml");
			//File myOutFile = new File("getstatement_y052_201102.xml");
			//File myOutFile = new File("memberDirectory_20110503.xml");
			//File myOutFile = new File("GrandmaMDay2011.xml");
			//File myOutFile = new File("MomMDay2011.xml");
			//File myOutFile = new File("MemberDirectory_20110507.xml");
			//File myOutFile = new File("UnReadMessages_test.xml");
			//File myOutFile = new File("Order_SequenceNumber_Example.xml");
			//File myOutFile = new File("MemberDirectory_20110524.xml");
			//File myOutFile = new File("FredSearch_request2.xml");
			//File myOutFile = new File("getstatement_x069_201105_adjusted.xml");
			//File myOutFile = new File("getstatement_x069_201106.xml");
			//File myOutFile = new File("order_dlcf_test_20.xml");
			//File myOutFile = new File("bms_order_resp1.xml");
			//File myOutFile = new File("order_audit_test.xml");
			//File myOutFile = new File("shop_available.xml");
			//File myOutFile = new File("E507_messages_on_outbound.xml");
			//File myOutFile = new File("MemberDirectory_20110811.xml");
			//File myOutFile = new File("Order_Acceptance.xml");
			//File myOutFile = new File("tlo_info.xml");
			//File myOutFile = new File("MemberDirectory_20110920.xml");
			//File myOutFile = new File("MemberDirectory_20111005.xml");
			//File myOutFile = new File("SuspendFromShop_03.xml");
			//File myOutFile = new File("MemberDirectoryQA_20111011.xml");
			//File myOutFile = new File("SecondChoiceProduct_rnd2a.xml");
			//File myOutFile = new File("MemberDirectory_20111031.xml");
			//File myOutFile = new File("BOM_Messaging_orderAccept.xml");
			//File myOutFile = new File("MemberDirectory_20111108.xml");
			//File myOutFile = new File("getstatement_x069_201110.xml");
			//File myOutFile = new File("getstatement_e507_201109.xml");
			//File myOutFile = new File("z999_qa_messages.xml");
			//File myOutFile = new File("getstatement_x069_201110.xml");
			//File myOutFile = new File("audittest.xml");
			//File myOutFile = new File("getstatement_w368_201110.xml");
			//File myOutFile = new File("MemberDirectory_20111202.xml");
			//File myOutFile = new File("MemberDirectory_20111206.xml");
			//File myOutFile = new File("MemberDirectory_20111221.xml");
			//File myOutFile = new File("MemberDirectory_20120107.xml");
			//File myOutFile = new File("getstatement_x069_201111.xml");
			//File myOutFile = new File("getstatement_x069_201112.xml");
			//File myOutFile = new File("MemberDirectory_20120119.xml");
			//File myOutFile = new File("MemberDirectory_20120131.xml");
			//File myOutFile = new File("MemberDirectory_20120206.xml");
			//File myOutFile = new File("getstatement_x069_201201.xml");
			//File myOutFile = new File("MemberDirectory_20120228.xml");
			//File myOutFile = new File("getstatement_x069_201202.xml");
			//File myOutFile = new File("dlcf_fix_checking2.xml");
			//File myOutFile = new File("MemberDirectory_20120403.xml");
			//File myOutFile = new File("getstatement_x069_201204.xml");
			//File myOutFile = new File("MemberDirectory_20120521.xml");
			//File myOutFile = new File("MemberDirectory_20120522_QA_new.xml");
			//File myOutFile = new File("MemberDirectory_wTLO_20120608.xml");
			//File myOutFile = new File("MemberDirectory_wTLO_20120629.xml");
			//File myOutFile = new File("disputeTest8.xml");
			//File myOutFile = new File("disputetest10.xml");
			//File myOutFile = new File("MemberDirectory_wTLO_20120727.xml");
			//File myOutFile = new File("getstatement_x069_201207.xml");
			//File myOutFile = new File("MemberDirectory_wTLO_20120905.xml");
			//File myOutFile = new File("TR_test_out1.xml");
			//File myOutFile = new File("MemberDirectory_wTLO_20120928.xml");
			File myOutFile = new File("orderout.xml");
			
			//READ FILE INTO A STRING
			StringBuffer buffer = new StringBuffer();
			FileInputStream myXMLStream = new FileInputStream(myXML);
			InputStreamReader ISR = new InputStreamReader(myXMLStream);
			Reader in = new BufferedReader(ISR);
			
			//LOOP THROUGH THE FILE AND ADD IT TO THE BUFFER
			int ch;
			while ((ch = in.read()) > -1) {
				buffer.append((char)ch);
			}
			in.close();
			
			//CONVERT BUFFER TO A STRING
			String myXMLString = buffer.toString();
				
			//CREATE OBJECT TO POST
			PostMethod myPost = new PostMethod(myURL);
			
			myPost.addParameter("func", myFunc);
			//myPost.addParameter("messageAckn", "false");
			//myPost.addParameter("messageAcknUsed", "true");
			myPost.addParameter("data", myXMLString);
			
			//System.out.println(myPost.getURI().toString());
			//System.out.println(myPost.getRequestURL().toString());
			
			//START BROWSER
			HttpClient myClient = new HttpClient();
			FileOutputStream outFile = null;
			InputStream myStream = null;
			
			//FEED THE FSI AND DISPLAY RESPONSE!
			try {
				int result = myClient.executeMethod(myPost);
				
				System.out.println("Response status code: " + result);
				
				System.out.println("Response Body: ");
				System.out.println("Getting response as Stream...");
				myStream = myPost.getResponseBodyAsStream();

				outFile = new FileOutputStream(myDir + myOutFile);
				int c;

	            while ((c = myStream.read()) != -1) {
	            	outFile.write(c);
	            }

	        } finally {
	            if (in != null) {
	                in.close();
	            }
	            if (outFile != null) {
	            	outFile.close();
	            }

				
				myPost.releaseConnection();
				System.out.println("Done!");
				
			}
			
		}
}
