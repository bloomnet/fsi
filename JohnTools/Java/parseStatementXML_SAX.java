import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
 
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class parseStatementXML_SAX {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		final String myDir = "C:\\Documents and Settings\\u0932\\My Documents\\aWork\\Software\\FSI\\STATEMENTS\\";

		
		//String myXMLFile = "fsi_getStatement_Response_B253_201101.xml";
		//String myXMLFile = "fsi_getStatement_Response_X069_201101.xml";
		//String myXMLFile = "X069201102.XML";
		String myXMLFile = "MemberDirectory_20110524.xml";
		
		//String myOutFile = "fsi_getStatement_Response_B253_201101";
		//String myOutFile = "X069_201101_Statement.csv";
		//final String myOutFile = "B253_201101_Statement2.csv";
		final String myOutFile = "X069_201102_Statement_JAVA.txt";

		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
		    SAXParser saxParser = factory.newSAXParser();
		     
		    DefaultHandler handler = new DefaultHandler() {
		    	 boolean myFound = false;
		         int i = 1;
		         String myElement = "";
		     
		         public void startElement(String uri, String localName,
		            String qName, Attributes attributes)
		            throws SAXException {
		     
		            if (qName.equalsIgnoreCase("GROSS_AMT_DSP") || qName.equalsIgnoreCase("EXTENDED_AMOUNT_DSP") ||qName.equalsIgnoreCase("SALES_ORDER")) {
		            	myFound = true;
		            	myElement = qName;
		            	System.out.println(i);
		            	i++;
		            }		     
		         }
		     
		         public void endElement(String uri, String localName,
		              String qName)
		              throws SAXException {
			        if (myFound) {
			        	myFound = false;
			        	myElement = "";
			        }
		         }
		     
		         public void characters(char ch[], int start, int length)
		             throws SAXException {
		     
		        	 try {
			        	 if (myFound) {
							FileOutputStream fos = new FileOutputStream(myDir + myOutFile,true);
							if (myElement.equalsIgnoreCase("GROSS_AMT_DSP")) {
								String grossAmount = new String(ch, start, length);
								grossAmount = grossAmount + "\t";
								fos.write(grossAmount.getBytes());
					          }
							 
							if (myElement.equalsIgnoreCase("EXTENDED_AMOUNT_DSP")) {
								String netAmount = new String(ch, start, length);
								netAmount = netAmount + "\t";
								fos.write(netAmount.getBytes());
							}
							 
							if (myElement.equalsIgnoreCase("SALES_ORDER")) {
								String orderNumber = new String(ch, start, length);
								orderNumber = orderNumber + "\n";
								fos.write(orderNumber.getBytes());
							}
							 
							fos.close();
			        	 }
						} catch (FileNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
		     
		            }
		     
		          };
		          
		     saxParser.parse(myDir + myXMLFile, handler);
		
		} catch (Exception e){
			e.printStackTrace();
		}
	}

}
