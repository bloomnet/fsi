package bloomnet.fsi.utilities;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.Hashtable;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class ParseAuditDetail {

	/**
	 * @param args
	 */
	final static String myDir = "C:\\BOM\\";

	final static String myOutFile = "V725_audit"+new Date().getTime()+".txt";

	static class myVariables {
		int myOrders = 0;
		int myX069 = 0;
		
	}

	public static void parseAuditDetail(String auditResponse){

		//System.out.println("in parseAudit");
		
		int myOrders = 0;
		int myX069 = 0;

		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();

			DefaultHandler handler = new DefaultHandler() {
				boolean bmtOrderNumber = false;
				boolean externalShopOrderNumber = false;
				boolean productDescription = false;
				boolean recipientFirstName = false;
				boolean messageStatus = false;
				boolean recipientLastName = false;
				boolean recipientPhoneNumber = false;
				boolean receivingShopCode = false;
				@SuppressWarnings("rawtypes")
				Hashtable myOrder = new Hashtable();
				int orders = 0;
				int x069 = 0;

				public void startElement(String uri, String localName,
						String qName, Attributes attributes)
				throws SAXException {

					bmtOrderNumber = false;
					externalShopOrderNumber = false;
					productDescription = false;
					recipientFirstName = false;
					messageStatus = false;
					recipientLastName = false;
					recipientPhoneNumber = false;
					receivingShopCode = false;

					if (qName.equalsIgnoreCase("messageOrder")) {
						myOrder.put("bmtOrderNumber", "");
						myOrder.put("externalShopOrderNumber", "");
						myOrder.put("messageStatus", "");
						myOrder.put("productDescription", "");
						myOrder.put("recipientFirstName", "");
						myOrder.put("recipientLastName", "");
						myOrder.put("recipientPhoneNumber", "");
						myOrder.put("receivingShopCode", "");
						
					}
					if (qName.equalsIgnoreCase("recipientFirstName")) {
						recipientFirstName = true;
					}
					if (qName.equalsIgnoreCase("messageStatus")) {
						messageStatus = true;
					}
					if (qName.equalsIgnoreCase("recipientLastName")) {
						recipientLastName = true;
					}
					if (qName.equalsIgnoreCase("bmtOrderNumber")) {
						bmtOrderNumber = true;
					}
					if (qName.equalsIgnoreCase("externalShopOrderNumber")) {
						externalShopOrderNumber = true;
					}
					if (qName.equalsIgnoreCase("recipientPhoneNumber")) {
						recipientPhoneNumber = true;
					}
					if (qName.equalsIgnoreCase("productDescription")) {
						productDescription = true;
					}
					if (qName.equalsIgnoreCase("receivingShopCode")) {
						receivingShopCode = true;
					}
				}

				public void endElement(String uri, String localName,
						String qName)
				throws SAXException {

					if (qName.equalsIgnoreCase("messageOrder")) {

						try {
							System.out.println(
									myOrder.get("receivingShopCode").toString() +
									"\t" +
									myOrder.get("bmtOrderNumber").toString() +
									"\t" +
									myOrder.get("externalShopOrderNumber").toString() +
									"\t" +
									myOrder.get("messageStatus").toString() +
									"\t" +
									myOrder.get("productDescription").toString() +
									"\t" +
									myOrder.get("recipientFirstName").toString() +
									" " +
									myOrder.get("recipientLastName").toString() +
									"\t" +
									myOrder.get("recipientPhoneNumber").toString() );	
							/*
							FileOutputStream fos;
							fos = new FileOutputStream(myDir + myOutFile,true);

							fos.write(myOrder.get("bmtOrderNumber").toString().getBytes());
							fos.write("\t".getBytes());
							fos.write(myOrder.get("messageStatus").toString().getBytes());
							fos.write("\t".getBytes());
							fos.write(myOrder.get("productDescription").toString().getBytes());
							fos.write("\t".getBytes());
							fos.write(myOrder.get("recipientFirstName").toString().getBytes());
							fos.write("\t".getBytes());
							fos.write(myOrder.get("recipientLastName").toString().getBytes());
							fos.write("\n".getBytes());
							fos.close();
							*/
							myOrder.clear();
						}
						catch (Exception e){
							
						}
					}
					}

				public void characters(char ch[], int start, int length)
				throws SAXException {
					if (recipientFirstName) {
						String myData = new String(ch, start, length);
						myOrder.put("recipientFirstName", myData);
						if(myData.equals("1")){
							orders++;
						}
						recipientFirstName = false;
					}
					if (messageStatus) {
						String myData = new String(ch, start, length);
						myOrder.put("messageStatus", myData);
						messageStatus = false;
					}
					if (recipientLastName) {
						String myData = new String(ch, start, length);
						myOrder.put("recipientLastName", myData);
						if(myData.equals("X0690000")){
							x069++;
						}
						recipientLastName = false;
					}
					if (bmtOrderNumber) {
						String myData = new String(ch, start, length);
						myOrder.put("bmtOrderNumber", myData);
						bmtOrderNumber = false;
					}
					if (externalShopOrderNumber) {
						String myData = new String(ch, start, length);
						myOrder.put("externalShopOrderNumber", myData);
						externalShopOrderNumber = false;
					}
					if (productDescription) {
						String myData = new String(ch, start, length);
						myOrder.put("productDescription", myData);
						productDescription = false;
					}
					if (recipientPhoneNumber) {
						String myData = new String(ch, start, length);
						myOrder.put("recipientPhoneNumber", myData);
						recipientPhoneNumber = false;
					}
					if (receivingShopCode) {
						String myData = new String(ch, start, length);
						myOrder.put("receivingShopCode", myData);
						receivingShopCode = false;
					}
				}
			};

			byte[] bytes = auditResponse.getBytes();
			InputStream myXMLStream = new ByteArrayInputStream(bytes);

			saxParser.parse(myXMLStream, handler);
			//System.out.println("Read " + jj + " messages");
			
			
		} catch (Exception e){
			e.printStackTrace();
		}
		

	}
	

	public static void main(String[] args) {

		
	}

}
