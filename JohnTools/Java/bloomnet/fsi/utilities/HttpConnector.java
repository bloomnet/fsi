package bloomnet.fsi.utilities;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class HttpConnector {

	public static HttpURLConnection get_connection(String url_string) {
		HttpURLConnection conn = null;
		String verb=FSIConstants.POST_VERB;
		try {
			URL url = new URL(url_string);
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod(verb);
		} catch (MalformedURLException e) {
			System.err.println(e);
		} catch (IOException e) {
			System.err.println(e);
		}
		return conn;
	}


	
}
