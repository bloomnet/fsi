package bloomnet.fsi.utilities;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.Hashtable;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class ParseAudit {

	/**
	 * @param args
	 */
	final static String myDir = "C:\\BOM\\";

	final static String myOutFile = "Z998_audit"+new Date().getTime()+".txt";
	//final static String myOutFile = "Z100_audit"+new Date().getTime()+".txt";

	static class myVariables {
		int myOrders = 0;
		int myX069 = 0;
		
	}

	public static void parseAudit(String auditResponse){

		System.out.println("in parseAudit");
		
		int myOrders = 0;
		int myX069 = 0;

		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();

			DefaultHandler handler = new DefaultHandler() {
				boolean message = false;
				boolean messageType = false;
				boolean messageStatus = false;
				boolean sendingShopCode = false;
				boolean receivingShopCode = false;
				boolean fulfillingShopCode = false;
				boolean bmtOrderNumber = false;
				boolean bmtSeqNumOfOrder = false;
				boolean bmtSeqNumOfMessage = false;
				boolean externalShopOrderNumber = false;
				boolean externalShopMessageNumber  = false;
				@SuppressWarnings("rawtypes")
				Hashtable myOrder = new Hashtable();
				int orders = 0;
				int x069 = 0;

				public void startElement(String uri, String localName,
						String qName, Attributes attributes)
				throws SAXException {

					message = false;
					messageType = false;
					messageStatus = false;
					sendingShopCode = false;
					receivingShopCode = false;
					fulfillingShopCode = false;
					bmtOrderNumber = false;
					bmtSeqNumOfOrder = false;
					bmtSeqNumOfMessage = false;
					externalShopOrderNumber = false;
					externalShopMessageNumber  = false;

					if (qName.equalsIgnoreCase("message")) {
						myOrder.put("messageType", "");
						myOrder.put("messageStatus", "");
						myOrder.put("sendingShopCode", "");
						myOrder.put("receivingShopCode", "");
						myOrder.put("fulfillingShopCode", "");
						myOrder.put("bmtOrderNumber", "");
						myOrder.put("bmtSeqNumOfOrder", "");
						myOrder.put("bmtSeqNumOfMessage", "");
						myOrder.put("externalShopOrderNumber", "");
						myOrder.put("externalShopMessageNumber", "");
						
					}
					if (qName.equalsIgnoreCase("messageType")) {
						messageType = true;
					}
					if (qName.equalsIgnoreCase("messageStatus")) {
						messageStatus = true;
					}
					if (qName.equalsIgnoreCase("sendingShopCode")) {
						sendingShopCode = true;
					}
					if (qName.equalsIgnoreCase("receivingShopCode")) {
						receivingShopCode = true;
					}
					if (qName.equalsIgnoreCase("fulfillingShopCode")) {
						fulfillingShopCode = true;
					}
					if (qName.equalsIgnoreCase("bmtOrderNumber")) {
						bmtOrderNumber = true;
					}
					if (qName.equalsIgnoreCase("bmtSeqNumOfOrder")) {
						bmtSeqNumOfOrder = true;
					}
					if (qName.equalsIgnoreCase("bmtSeqNumOfMessage")) {
						bmtSeqNumOfMessage = true;
					}
					if (qName.equalsIgnoreCase("externalShopOrderNumber")) {
						externalShopOrderNumber = true;
					}
					if (qName.equalsIgnoreCase("externalShopMessageNumber")) {
						externalShopMessageNumber = true;
					}
				}

				public void endElement(String uri, String localName,
						String qName)
				throws SAXException {

					if (qName.equalsIgnoreCase("message")) {

						try {
								
							FileOutputStream fos;
							fos = new FileOutputStream(myDir + myOutFile,true);

							fos.write(myOrder.get("messageType").toString().getBytes());
							fos.write("\t".getBytes());
							fos.write(myOrder.get("messageStatus").toString().getBytes());
							fos.write("\t".getBytes());
							fos.write(myOrder.get("sendingShopCode").toString().getBytes());
							fos.write("\t".getBytes());
							fos.write(myOrder.get("receivingShopCode").toString().getBytes());
							fos.write("\t".getBytes());
							fos.write(myOrder.get("fulfillingShopCode").toString().getBytes());
							fos.write("\t".getBytes());
							fos.write(myOrder.get("bmtOrderNumber").toString().getBytes());
							fos.write("\t".getBytes());
							fos.write(myOrder.get("bmtSeqNumOfOrder").toString().getBytes());
							fos.write("\t".getBytes());
							fos.write(myOrder.get("bmtSeqNumOfMessage").toString().getBytes());
							fos.write("\t".getBytes());
							fos.write(myOrder.get("externalShopOrderNumber").toString().getBytes());
							fos.write("\t".getBytes());
							fos.write(myOrder.get("externalShopMessageNumber").toString().getBytes());
							fos.write("\n".getBytes());
							fos.close();
							myOrder.clear();
						}
						catch (Exception e){
							
						}
					}
					}

				public void characters(char ch[], int start, int length)
				throws SAXException {
					if (messageType) {
						String myData = new String(ch, start, length);
						myOrder.put("messageType", myData);
						if(myData.equals("1")){
							orders++;
						}
						messageType = false;
					}
					if (messageStatus) {
						String myData = new String(ch, start, length);
						myOrder.put("messageStatus", myData);
						messageStatus = false;
					}
					if (sendingShopCode) {
						String myData = new String(ch, start, length);
						myOrder.put("sendingShopCode", myData);
						if(myData.equals("X0690000")){
							x069++;
						}
						sendingShopCode = false;
					}
					if (receivingShopCode) {
						String myData = new String(ch, start, length);
						myOrder.put("receivingShopCode", myData);
						receivingShopCode = false;
					}
					if (fulfillingShopCode) {
						String myData = new String(ch, start, length);
						myOrder.put("fulfillingShopCode", myData);
						fulfillingShopCode = false;
					}
					if (bmtOrderNumber) {
						String myData = new String(ch, start, length);
						myOrder.put("bmtOrderNumber", myData);
						bmtOrderNumber = false;
					}
					if (bmtSeqNumOfOrder) {
						String myData = new String(ch, start, length);
						myOrder.put("bmtSeqNumOfOrder", myData);
						bmtSeqNumOfOrder = false;
					}
					if (bmtSeqNumOfMessage) {
						String myData = new String(ch, start, length);
						myOrder.put("bmtSeqNumOfMessage", myData);
						bmtSeqNumOfMessage = false;
					}
					if (externalShopOrderNumber) {
						String myData = new String(ch, start, length);
						myOrder.put("externalShopOrderNumber", myData);
						externalShopOrderNumber = false;
					}
					if (externalShopMessageNumber) {
						String myData = new String(ch, start, length);
						myOrder.put("externalShopMessageNumber", myData);
						externalShopMessageNumber = false;
					}
				}
			};

			byte[] bytes = auditResponse.getBytes();
			InputStream myXMLStream = new ByteArrayInputStream(bytes);

			saxParser.parse(myXMLStream, handler);
			//System.out.println("Read " + jj + " messages");
			
			
		} catch (Exception e){
			e.printStackTrace();
		}
		

	}
	

	public static void main(String[] args) {

		
	}

}
