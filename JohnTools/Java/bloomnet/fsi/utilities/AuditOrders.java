package bloomnet.fsi.utilities;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URLEncoder;

import bloomnet.fsi.utilities.HttpConnector;
import bloomnet.fsi.utilities.ParseAudit;

public class AuditOrders {
	
		

	public static void main(String[] args) {

		String myDate1 = "20120511000000";
		String myDate2 = "20120514000000";
		
		String myShop = "Z9980000";
		String myUser = "Z998";
		String myPassword = "bloomnet2011";

		/*
		String myShop = "Z1000000";
		String myUser = "Z100";
		String myPassword = "BLOOMORA";

		String myShop = "Z9990000";
		String myUser = "Z999";
		String myPassword = "bloomnet2011";
		 
		String myShop = "X0690000";
		String myUser = "DIALER";
		String myPassword = "TRUNK";
		String myShop = "V7250000";
		String myUser = "V725";
		String myPassword = "PNET";
		String myShop = "B2530000";
		String myUser = "B253";
		String myPassword = "qa";
		*/
		String data = "";
		String post = "";
		
			data = "<auditInterface>"
			+ "<auditRequest>"
			+ "<security>"
			+ "<shopCode>" + myShop + "</shopCode>"
			+ "<username>" + myUser + "</username>"
			+ "<password>" + myPassword + "</password>"
			+ "</security>"
			+ "<auditSearchOptions>"
			+ "<auditMessagesByDateRange>"
			+ "<messageDirection>INBOUND</messageDirection>"
			+ "<startDate>" + myDate1 + "</startDate>"
			+ "<endDate>" + myDate2 + "</endDate>"
			+ "</auditMessagesByDateRange>"
			+ "</auditSearchOptions>"
			+ "</auditRequest>"
			+ "</auditInterface>";
			
			String dataEncoded;
			try {
				dataEncoded = URLEncoder.encode(data, "UTF-8");
			
			
				post = FSIConstants.PROD_ENPOINT + FSIConstants.GET_AUDITINFO + dataEncoded;
			//	post = FSIConstants.QA2_ENDPOINT + FSIConstants.GET_AUDITINFO + dataEncoded;
			
	        
				HttpURLConnection conn = HttpConnector.get_connection(post);
	        try {
				conn.connect();

	        String xml = "";

	        BufferedReader reader;
				try {
					reader = new BufferedReader(new InputStreamReader(
							conn.getInputStream()));

					String next = null;
					while ((next = reader.readLine()) != null)
						xml += next;

					System.out.println(xml);
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				ParseAudit.parseAudit(xml);
			
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}

}
