package bloomnet.fsi.utilities;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import bloomnet.fsi.utilities.HttpConnector;
import bloomnet.fsi.utilities.ParseAuditDetail;

public class AuditOrdersDetail {
	
		

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {

		String[] myOrders1 = {"28687056","28687211"};
		String[] myOrders2 = {"28686549","28686553","28686555","28687306","28687313"};
		String[] myOrders3 = {"28686656","28686951","28687025","28687026","28687027","28687028","28687029","28687030","28687032","28687404","28687706","28687711","28687728","28687742","28687743","28909298","28909299","28909300","28909301","28909302"};
		String[] myOrders = {"28686596","28686627","28686979","28687002","28687048","28687250","28687252","28687352","28687375","28687401","28687402"};

		
		String[] myShops ={"V3660000","V5560000","V7250000","V7270000"};
		String[] myUsers ={"V366","V556","V725","V727"};
		String[] myPasswords ={"catdog","SIT","PNET","NFP"};
		
		int jj = 3;
		
		String myShop = myShops[jj];
		String myUser = myUsers[jj];
		String myPassword = myPasswords[jj];
		/*
		String myShop = "Z9990000";
		String myUser = "Z999";
		String myPassword = "bloomnet2011";
		String myShop = "Z9980000";
		String myUser = "Z998";
		String myPassword = "bloomnet2011";
		String myShop = "B2530000";
		String myUser = "B253";
		String myPassword = "qa";
		*/
		String data = "";
		String post = "";
		for( int i = 0; i < myOrders.length; i++ ) {
			data = "<auditInterface>"
			+ "<auditRequest>"
			+ "<security>"
			+ "<shopCode>" + myShop + "</shopCode>"
			+ "<username>" + myUser + "</username>"
			+ "<password>" + myPassword + "</password>"
			+ "</security>"
			+ "<auditSearchOptions>"
			+ "<auditDetailedInfoOnOrderByOrderNumber>"
			+ "<messageDirection>BOTH</messageDirection>"
			+ "<orderNumberType>internal</orderNumberType>"
			+ "<orderNumber>" + myOrders[i] + "</orderNumber>"
			+ "</auditDetailedInfoOnOrderByOrderNumber>"
			+ "</auditSearchOptions>"
			+ "</auditRequest>"
			+ "</auditInterface>";
			
			String dataEncoded;
			try {
				dataEncoded = URLEncoder.encode(data, "UTF-8");
			
			
				post = FSIConstants.PROD_ENPOINT + FSIConstants.GET_AUDITINFO + dataEncoded;
			//	post = FSIConstants.QA2_ENDPOINT + FSIConstants.GET_AUDITINFO + dataEncoded;
			
	        
				HttpURLConnection conn = HttpConnector.get_connection(post);
	        try {
				conn.connect();

	        String xml = "";

	        BufferedReader reader;
				try {
					reader = new BufferedReader(new InputStreamReader(
							conn.getInputStream()));

					String next = null;
					while ((next = reader.readLine()) != null)
						xml += next;

					//System.out.println(xml);
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				ParseAuditDetail.parseAuditDetail(xml);
			
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}
