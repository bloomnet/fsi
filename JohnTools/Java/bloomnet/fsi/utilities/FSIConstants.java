package bloomnet.fsi.utilities;

public final class FSIConstants {

	// bloomLink environments
	public static final String QA_ENDPOINT = "http://10.180.1.220:9081/fsiv2/processor";
	public static final String QA2_ENDPOINT = "http://qa.bloomlink.net/fsiv2/processor";
	public static final String PROD_ENPOINT = "http://www.bloomlink.net/fsiv2/processor";

	public static final String ORDER_MESSAGE_TYPE = "0";
	public static final String INQR_MESSAGE_TYPE = "1";
	public static final String RESP_MESSAGE_TYPE = "2";
	public static final String RJCT_MESSAGE_TYPE = "4";
	public static final String CANC_MESSAGE_TYPE = "3";
	public static final String DENI_MESSAGE_TYPE = "6";
	public static final String DLCF_MESSAGE_TYPE = "7";
	public static final String DLOU_MESSAGE_TYPE = "8";
	public static final String CONF_MESSAGE_TYPE = "11";
	public static final String INFO_MESSAGE_TYPE = "12";
	public static final String PCHG_MESSAGE_TYPE = "18";
	public static final String ACKF_MESSAGE_TYPE = "24";
	public static final String DLCA_MESSAGE_TYPE = "26";

	// TODO what is dlca numeric type

	// Broadcast from bmt, not a message on order
	public static final String MSG_MESSAGE_TYPE = "19";

	public static final String OFF_MESSAGE_TYPE = "0";

	public static final String SYSTEM_TYPE = "GENERAL";
	public static final String WIRE_SERVICE_CODE = "BMT";

	// message types
	public static final String POST_MESSAGE = "?func=postmessages&data=";
	public static final String GET_MESSAGE = "?func=getmessages&messageAckn=true&data=";
	public static final String GET_STATEMENT = "?func=getStatement&data=";
	public static final String GET_MEMBER = "?func=getMemberDirectory&data=";
	public static final String GET_AUDITINFO = "?func=getAuditInfo&data=";

	public static final String POST_VERB = "POST";

	public static final String NUM_GENERAL_MESSAGES = "fsi.maxNumOfGeneralMessages";
	public static final String NUM_ACKF_MESSAGES = "fsi.maxNumOfAckfMessages";
	public static final String NUM_ORDER_MESSAGES = "fsi.maxNumOfOrderMessages";

	// Occasion codes
	public static final String Funeral = "1";
	public static final String Illness = "2";
	public static final String Birthday = "3";
	public static final String BusinessGifts = "4";
	public static final String Holiday = "5";
	public static final String Maternity = "6";
	public static final String Anniversary = "7";
	public static final String Others = "8";

	// FSI properties
	public static final String FSI_SHOPCODE = "fsi.shopcode";
	public static final String FSI_PASSWORD = "fsi.password";
	public static final String FSI_USERNAME = "fsi.username";
	public static final String NUM_OF_SHOP_ATTEMPTS = "fsi.numOfShopAttempts";
	public static final String NUM_OF_SHOP_ATTEMPTS_SAME_DAY = "fsi.numOfShopAttemptsSameDay";
	public static final String FSI_SHOP_NOT_FOUND = "fsi.shopNotFound";

	// Order type abbreviations
	public static final String ORDER_MESSAGE_DESC = "ORDER";
	public static final String INQR_MESSAGE_DESC = "INQR";
	public static final String RESP_MESSAGE_DESC = "RESP";
	public static final String RJCT_MESSAGE_DESC = "RJCT";
	public static final String CANC_MESSAGE_DESC = "CANC";
	public static final String DENI_MESSAGE_DESC = "DENI";
	public static final String DLCF_MESSAGE_DESC = "DLCF";
	public static final String DLCA_MESSAGE_DESC = "DLCA";
	public static final String DLOU_MESSAGE_DESC = "DLOU";
	public static final String CONF_MESSAGE_DESC = "CONF";
	public static final String INFO_MESSAGE_DESC = "INFO";
	public static final String PCHG_MESSAGE_DESC = "PCHG";
	public static final String ACKF_MESSAGE_DESC = "ACKF";
	
	//DLCA reason for non delivery
	public static final String DLCA_NEED_INFO = "94";
	public static final String DLCA_NOT_AVAIL = "95";
	public static final String DLCA_INC_DATE = "96";
	public static final String DLCA_OTHER = "97";
	
	//DLCA additional reason for non delivery
	public static final String DLCA_WILL_ATTEMPT = "99";
	public static final String DLCA_WILL_ATTEMPT_CONT = "98";
	
}
