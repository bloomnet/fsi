 select 
 IF(count(sn1.shop_id)=3,'All 3',
	IF(count(sn1.shop_id)=1 AND sn1.network_id=1,'Just BloomNet',
	IF(count(sn1.shop_id)=1 AND sn1.network_id=2,'Just TF',
	IF(count(sn1.shop_id)=1 AND sn1.network_id=3,'Just FTD',
		IF(count(sn1.shop_id)=2 AND sn1.shop_id not in (select sn2.shop_id from shopnetwork sn2 where sn2.network_id = 3),'BloomNet and TF',
		IF(count(sn1.shop_id)=2 AND sn1.shop_id not in (select sn2.shop_id from shopnetwork sn2 where sn2.network_id = 2),'BloomNet and FTD',
		IF(count(sn1.shop_id)=2 AND sn1.shop_id not in (select sn2.shop_id from shopnetwork sn2 where sn2.network_id = 1),'TF and FTD',
	'Other'))))))) as 'Network Membership',
             GROUP_CONCAT(DISTINCT IF(sn1.Network_ID=1,sn1.ShopCode,"")) as 'BloomNet', 
  GROUP_CONCAT(DISTINCT IF(sn1.Network_ID=2,sn1.ShopCode,"")) as 'Teleflora',
  GROUP_CONCAT(DISTINCT IF(sn1.Network_ID=3,sn1.ShopCode,"")) as 'FTD',

          sh.ShopName,
        sh.ShopAddress1,
        sh.ShopAddress2,
        sh.ShopPhone,
        sh.ShopContact,
        sh.shopfax,
        sh.Shop800,
        sh.shopemail,
        c.Name as 'City',
        s.Short_Name as 'State',
        z.Zip_Code as 'ZipCode'


    from shopdata.shopnetwork sn1
    join shop sh on sn1.shop_id = sh.shop_id
    inner join city c on sh.City_ID = c.City_ID
inner join state s on c.State_ID = s.State_ID
inner join zip z on sh.Zip_ID = z.Zip_ID
  group by sn1.shop_id
  ;
  
