select
        bo.ordernumber, 
        bo.bomorder_id, 
        bo.parentorder_id,
        bo2.OrderNumber as 'Parent order',
        SUBSTRING(bo.orderxml, 
      LOCATE('<recipientLastName>', bo.orderxml)+19, 
      LOCATE('</recipientLastName>', bo.orderxml) - LOCATE('<recipientLastName>', bo.orderxml) - 19) 
    AS recipientLastName,
        oa.CreatedDate, 
        oa.User_ID, 
        ap.PaymentAmount, 
        ap.PayTo, 
        pt.Description as 'Recorded Payment Type',
          GROUP_CONCAT(DISTINCT IF(sn.Network_ID=1,sn.ShopCode,"")) as 'BloomNet', 
  GROUP_CONCAT(DISTINCT IF(sn.Network_ID=2,sn.ShopCode,"")) as 'Teleflora',
  GROUP_CONCAT(DISTINCT IF(sn.Network_ID=3,sn.ShopCode,"")) as 'FTD',
  GROUP_CONCAT(DISTINCT IF(sn.Network_ID=4,sn.ShopCode,"")) as 'Mom&Pop',
        sh.ShopName,
        sh.ShopAddress1,
        sh.ShopAddress2,
        sh.ShopPhone,
        sh.ShopContact,
        sh.shopfax,
        sh.Shop800,
        sh.shopemail,
        c.Name as 'City',
        s.Short_Name as 'State',
        z.Zip_Code as 'ZipCode'

FROM bomorder bo
inner join bomorder bo2 on bo.parentorder_id = bo2.BOMOrder_ID
right outer join orderactivity oa on oa.BOMOrder_ID = bo.BOMOrder_ID
right outer join oactivitytype oat ON oa.OActivityType_ID = oat.OActivityType_ID
right outer join act_payment ap on ap.OrderActivity_ID = oa.OrderActivity_ID
right outer join paymenttype pt ON ap.PaymentType_ID = pt.PaymentType_ID
inner join shop sh on bo.ReceivingShop_ID = sh.Shop_ID
right outer join shopnetwork sn on sn.Shop_ID = sh.Shop_ID
inner join city c on sh.City_ID = c.City_ID
inner join state s on c.State_ID = s.State_ID
inner join zip z on sh.Zip_ID = z.Zip_ID
where
-- bo2.OrderNumber = '29312024'

oa.CreatedDate >= '2012-09-01'
and
oa.CreatedDate < '2012-10-01'



group by bo.ordernumber
order by oa.CreatedDate desc;

