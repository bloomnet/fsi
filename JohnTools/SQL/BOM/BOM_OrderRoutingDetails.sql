/*

*/

use bloomnetordermanagement;

 SET @myOrderId = '';
-- select bomorder_id into @myOrderId from bomorder where ordernumber = '34511166'; -- 13147
 select bo1.bomorder_id into @myOrderId from bomorder bo1 where bo1.BOMOrder_ID in (
 select bo2.parentorder_id from bomorder bo2 where bo2.ordernumber = 'WCV307'); 

-- SET @myOrderId = 16728;

select bomorder.BOMOrder_ID, ParentOrder_ID, bomorder.ordernumber, bomorder.CreatedDate, bomorder.User_ID, s.ShopName, s.ShopPhone  
from bomorder
inner join shop s on bomorder.ReceivingShop_ID = s.Shop_ID
where bomorder.ParentOrder_ID = @myOrderId;

select bo.ordernumber, bo.bomorder_id, oa.CreatedDate, oa.User_ID, ar.VirtualQueue, sr.Description FROM bomorder bo
right outer join orderactivity oa on oa.BOMOrder_ID = bo.BOMOrder_ID
right outer join oactivitytype oat ON oa.OActivityType_ID = oat.OActivityType_ID
right outer join act_routing ar on ar.OrderActivity_ID = oa.OrderActivity_ID
right outer join sts_routing sr ON ar.Sts_Routing_ID = sr.Sts_Routing_ID
where bo.BOMOrder_ID = @myOrderId
order by oa.CreatedDate desc;

select bomorder.BOMOrder_ID, ParentOrder_ID, bomorder.ordernumber, bomorder.CreatedDate, bomorder.DeliveryDate, bomorder.User_ID from bomorder where bomorder.BOMOrder_ID = @myOrderId;

select bo.ordernumber, bo.bomorder_id, oa.CreatedDate, oa.User_ID, al.LogACallText FROM bomorder bo
right outer join orderactivity oa on oa.BOMOrder_ID = bo.BOMOrder_ID
right outer join oactivitytype oat ON oa.OActivityType_ID = oat.OActivityType_ID
right outer join act_logacall al ON al.OrderActivity_ID = oa.OrderActivity_ID
where bo.BOMOrder_ID = @myOrderId
order by oa.CreatedDate;

select bo.ordernumber, bo.bomorder_id, oa.CreatedDate, oa.User_ID, ap.PaymentAmount, ap.PayTo, pt.Description FROM bomorder bo
right outer join orderactivity oa on oa.BOMOrder_ID = bo.BOMOrder_ID
right outer join oactivitytype oat ON oa.OActivityType_ID = oat.OActivityType_ID
right outer join act_payment ap on ap.OrderActivity_ID = oa.OrderActivity_ID
right outer join paymenttype pt ON ap.PaymentType_ID = pt.PaymentType_ID
where bo.BOMOrder_ID = @myOrderId
order by oa.CreatedDate;

select oa.BOMOrder_ID, oa.OActivityType_ID, am.MessageXML, am.SendingShop_ID, bo.ordernumber, bo.bomorder_id, oa.CreatedDate, oa.User_ID, oa.OrderActivity_ID, am.Sts_Message_ID, mt.ShortDesc, am.MessageText FROM bomorder bo
right outer join orderactivity oa on oa.BOMOrder_ID = bo.BOMOrder_ID
right outer join oactivitytype oat ON oa.OActivityType_ID = oat.OActivityType_ID
right outer join act_message am on am.OrderActivity_ID = oa.OrderActivity_ID
inner join messagetype mt on am.MessageType_ID = mt.MessageType_ID
where bo.BOMOrder_ID = @myOrderId
order by oa.CreatedDate;

select bo.ordernumber, bo.bomorder_id, oa.CreatedDate, oa.User_ID, aa.* FROM bomorder bo
right outer join orderactivity oa on oa.BOMOrder_ID = bo.BOMOrder_ID
right outer join oactivitytype oat ON oa.OActivityType_ID = oat.OActivityType_ID
right outer join act_audit aa on aa.OrderActivity_ID = oa.OrderActivity_ID
where bo.BOMOrder_ID = @myOrderId
order by oa.CreatedDate;

select bo.ordernumber, bo.bomorder_id, z.zip_code, c.name, s.Short_Name FROM bomorder bo
inner join zip z on bo.Zip_ID = z.Zip_ID
inner join city c on bo.City_ID = c.City_ID
inner join state s on c.State_ID = s.State_ID
where bo.BOMOrder_ID = @myOrderId
;

