use bloomnet;
-- SET @todaysDate = DATE_FORMAT(Now(),'%Y-%m-%d');
SET @todaysDate = '2012-10-31';
-- SET @tomorDate = '2012-10-30';

SELECT id,
       tracking_number,
       DATE_SUB((FROM_UNIXTIME(timestamp/1000)), INTERVAL 5 HOUR) as 'OrderDate',
       message,
       delivery_date,
       fulfilling_shop,
       receiving_shop,
       recipient_address,
       recipient_name,
       sending_shop
  FROM bloomnet.processed_order
  where 
  -- fulfilling_shop != 'Z9980000' and
  tracking_number != 0
  and DATE_SUB((FROM_UNIXTIME(timestamp/1000)), INTERVAL 5 HOUR) > @todaysDate
  -- and DATE_SUB((FROM_UNIXTIME(timestamp/1000)), INTERVAL 5 HOUR) <= @tomorDate
  order by tracking_number